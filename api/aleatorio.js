/*
    Lista um pokémon aleatorio da lista
*/

var data = require('../data')

module.exports = function (req, res) {
    const randomIndex = Math.floor(Math.random() * (data.length - 1)) + 1;

    res.json(data[randomIndex]);
}